$LOAD_PATH.unshift File.expand_path("../", __FILE__)

require "test_helper"

def get_next_state(alive: , alive_neighbors:)

  if alive_neighbors == 3
    return true
  end
  if alive && alive_neighbors == 2
    return true
    end
  return false
end

def get_is_alive(board:, x:, y:)
  return board[x][y]
end

def get_alive_neighbor_count(board:, x:, y:)
  if board[0][0]
  return 0
end

class CodeDayTest < Minitest::Test
  def test_under_population
    result = get_next_state(alive: true,alive_neighbors: 1)
    assert result == false

  end
  def test_survival_two_neighbors
    result = get_next_state(alive: true, alive_neighbors: 2)
    assert result == true
  end

  def test_survival_three_neighbors
    result = get_next_state(alive: true, alive_neighbors: 3)
    assert result == true
  end

  def test_overcrowding_4
    result = get_next_state(alive: true, alive_neighbors: 4)
    assert result == false
  end

  def test_overcrowding_dead
    result = get_next_state(alive: false, alive_neighbors: 4)
    assert result == false
  end

  def test_reproduction_3
    result = get_next_state(alive: false, alive_neighbors: 3)
    assert result == true
  end

  def test_no_reproduction_2
    result = get_next_state(alive: false, alive_neighbors: 2)
    assert result == false
  end

  def test_is_alive_live_cell
    board = [[false, false], [true, true]]
    result = get_is_alive(board: board, x: 1, y: 1)
    assert result == true
  end

  def test_is_alive_dead_cell
    board = [[false, false], [true, true]]
    result = get_is_alive(board: board, x: 0, y: 0)
    assert result == false
  end
  def test_alive_neighbor_count
    board = [[false,false,false],[false,false,false],[false,false,false] ]
    result = get_alive_neighbor_count(board: board,x:1,y:1)
    assert result == 0

  end

  def test_alive_neighbor_count_eight
    board = [[true, true, true],[true, true, true],[true, true, true] ]
    result = get_alive_neighbor_count(board: board,x:1,y:1)
    assert result == 8

  end

end

